(ns test-task.core
  (:require [org.httpkit.client :as http]
            [clojure.data.json :as json])
  (:gen-class))

; ---------------- First task

(def access-token "72cc26eef0dee791ebea5fb572c90795683d250499a1715be3dd2424adf39935")

(def options
  {:timeout 30000                                           ; ms
   :headers {"Authorization" (str "Bearer " access-token)}})

(defn followers [id]
  (str "/users/" id "/followers"))

(defn shots [id]
  (str "/users/" id "/shots"))

(defn likes [id]
  (str "/shots/" id "/likes"))

(defn call-dribble [uri]
  (println (str "loading " uri))
  (http/get (str "https://api.dribbble.com/v1" uri) options))

(defn extract-call-body [call]
  (let [called @call]
    (println (str "loaded " (-> called :opts :url)))
    (-> called :body json/read-str)))

(defn likers [id]
  (let [call-followers (call-dribble (followers id))
        follower-ids (->> call-followers
                          extract-call-body
                          (map #(get-in % ["follower" "id"])))
        calls-shots (doall (map #(call-dribble (shots %)) follower-ids))
        calls-likes (doall (mapcat (fn [call-shots]
                                     (let [call-likes (->> call-shots
                                                           extract-call-body
                                                           (map #(call-dribble (likes (% "id")))))]
                                       call-likes)) calls-shots))
        collected-ids (doall (mapcat (fn [call-likes] (->> call-likes
                                                           extract-call-body
                                                           (map #(get-in % ["user" "id"])))) calls-likes))]
    (frequencies collected-ids)))

; ---------------- Second task

(defn new-pattern [pattern]
  (let [chunks (clojure.string/split pattern #" ")
        prep (map #(rest (re-find #"(\S*)\((\S*)\);" %)) chunks)]
    prep))

(defn destruct [url]
  (let [main-parts (clojure.string/split url #"\?")
        idx (+ 3 (clojure.string/index-of (first main-parts) "://"))
        removed-head (subs (first main-parts) idx)
        parts (clojure.string/split removed-head #"/")
        pairs (if (= 1 (count main-parts))
                []
                (map #(clojure.string/split % #"=") (clojure.string/split (last main-parts) #"&")))]
    {:host   (first parts)
     :path   (rest parts)
     :params (into {} (for [[k v] pairs]
                        [(keyword k) v]))}))

(defn recognize [pattern url]
  (let [destructed (destruct url)
        prms (for [[k v] pattern]
               (case (keyword k)
                 :host (if (= (:host destructed) v) {} nil)
                 :path (let [path (:path destructed)
                             parts (clojure.string/split v #"/")]
                         (if (not= (count path) (count parts))
                           nil
                           (let [pairs (zipmap parts path)
                                 pred (fn [s] (clojure.string/starts-with? s "?"))
                                 non-match (filter (fn [[k v]] (not (pred k))) pairs)
                                 match (->> pairs
                                            (filter (fn [[k v]] (pred k)))
                                            (map (fn [[k v]] [(keyword (subs k 1)) v])))]
                             (if (every? (fn [[k v]] (= k v)) non-match)
                               (into {} match)
                               nil))))
                 :queryparam (let [[name val] (clojure.string/split v #"=\?")
                                   param (get-in destructed [:params (keyword name)])]
                               (if (nil? param) nil {(keyword val) param}))))]
    (if (every? #(not (nil? %)) prms)
      (into {} prms)
      nil)))

(def twitter (new-pattern "host(twitter.com); path(?user/status/?id);"))
(def dribbble (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);"))
(def dribbble2 (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);"))

(defn -main
  [& args]
  (let [id "sarai-palomino"
        freqs (likers id)]
    (println freqs)
    (println (take 10 (sort-by #(- (freqs %)) (keys freqs))))
    (println (recognize twitter "http://twitter.com/bradfitz/status/562360748727611392"))
    (println (recognize dribbble2 "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"))
    (println (recognize dribbble "https://twitter.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"))
    (println (recognize dribbble "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users"))))